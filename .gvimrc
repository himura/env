colorscheme desert
set guifont=JetBrains_Mono:h12
set guioptions-=T "remove toolbar
set go-=m         "remove menu bar
set lines=20
set columns=120
set clipboard=unnamed
