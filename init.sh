#!/bin/bash

git_config_alias() {  # copypaste-friendly

git config --global alias.it '!git init && git commit -m "root" --allow-empty';
git config --global alias.st 'status --short --branch';
git config --global alias.lg "log --graph --author-date-order --abbrev-commit --decorate --all --format=format:'%C(bold yellow)%d%C(reset) %s - %C(bold cyan)%ad%C(reset) - %an %C(bold blue)[%h]%C(reset)' --date=format:'%d %b %Y %H:%M'";
git config --global alias.ds '!git diff --staged && echo Diffs OK? && read _';
git config --global alias.dcp '!git ds && git commit && git push';
git config --global alias.c '!git pull && git add . && git dcp';
git config --global alias.mr '!git pull && git add . && git ds && git switch -c ${1?} && git commit && git push --set-upstream origin $1 && echo';
git config --global alias.pull-force '!git stash && git pull && git stash pop';
git config --global alias.please 'push --force-with-lease';
git config --global alias.submodule-remove '!git submodule deinit -f "${1%/}" && git rm --cached -f "${1%/}" && rm -rf ".git/modules/$1" && echo';
git config --global alias.ff '!git fetch origin ${1-main}:${1-main} && echo';
git config --global alias.fork-set '!git remote add -f fork ${1?} && git remote rename origin upstream && git remote rename fork origin'

# In PowerShell, use the commented one
# git config --global alias.branch-prune '!git fetch -p && for b in $(git for-each-ref --format=''%(if:equals=[gone])%(upstream:track)%(then)%(refname:short)%(end)'' refs/heads); do git branch -d $b; done'

git config --global alias.branch-prune '!git fetch -p && for b in $(git for-each-ref --format='\''%(if:equals=[gone])%(upstream:track)%(then)%(refname:short)%(end)'\'' refs/heads); do git branch -d $b; done';

echo 'Git aliases configured.'
}
git_config_user() {
    git config --global user.name "Himura Kazuto"
    git config --global user.email "$(echo 'Z2xhZ29sMTVAZ21haWwuY29tCg==' | base64 -d)"
    git config --global help.autocorrect 20
    echo 'Git user configured.'
}
update_config() {
    hash wget || exit 1
    echo "Fetching ~/$1..."
    if [ -f ~/$1 ]
    then
        bkp_name="backup-$(date +%s)-$1"
        echo "WARNING: Found existing ~/$1. Backing it up as ~/$bkp_name"
        mv ~/$1 ~/$bkp_name
    fi
    wget -q -P ~ https://gitlab.com/himura/env/raw/master/$1
}
prepare_bashrc() {
    if [ ! -f  ~/.bashrc ]
    then
        echo 'No ~/.bashrc! Restoring...'
        cp /etc/skel/.bashrc ~/
    fi
    bash_alias_header="# Himura's shortcuts"
    if ! grep -q "^$bash_alias_header" ~/.bashrc
    then
        printf "\n\n%s\n" "$bash_alias_header" >> ~/.bashrc
    fi
}
ensure_bashrc_line() {
    if grep -q "^${1//\n/\\n}\$" ~/.bashrc
    then
        echo "Line '$1' is already in ~/.bashrc."
    else
        echo "$1" >> ~/.bashrc
        echo "Line '$1' added to ~/.bashrc."
    fi
}
main() {
    hash git && git_config_alias && git_config_user
    hash vim && update_config .vimrc
    hash top && update_config .toprc
    hash tmux && update_config .tmux.conf

    prepare_bashrc
    ensure_bashrc_line "alias l='ls -CF'"
    ensure_bashrc_line "alias ll='ls -hAlF'"
    ensure_bashrc_line 'PS1="\n$PS1"'

    . ~/.bashrc
    hash vim && hash update-alternatives && echo 'TODO: sudo update-alternatives --config editor'
}
echo
echo 'Starting the script...'
main
