PowerShellGet\Install-Module posh-git -Scope CurrentUser -Force
Get-Content $PROFILE.CurrentUserAllHosts
@'
function g {
    param (
        [Parameter(ValueFromRemainingArguments)] [string[]] $p = 'status'
    )
    Write-Host "& git $p"
    & git $p
    Import-Module posh-git
}
function  {  # Ctrl + D
    exit
}
'@ > $PROFILE.CurrentUserAllHosts
