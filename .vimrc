set nocp
set mouse-=a

set number
set numberwidth=2

set ignorecase smartcase
set incsearch

nnoremap j gj
nnoremap k gk
set scrolloff=9

inoremap <C-BS> <C-W>
set tabstop=8 softtabstop=0 expandtab shiftwidth=2 smarttab

set background=dark
syntax on
